FROM docker.io/library/golang:1.18-alpine AS build
WORKDIR /build
RUN apk add --no-cache git
COPY . .
RUN apk add --no-cache ca-certificates
RUN CGO_ENABLED=0 go build -v -installsuffix 'static' ./cmd/relay

FROM scratch AS runtime
ENTRYPOINT [ "/relay" ]
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/relay /
