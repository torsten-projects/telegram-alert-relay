package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"telegram-alert-relay/internal/config"
	"telegram-alert-relay/internal/metrics"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type alert struct {
	Status string            `json:"status"`
	Labels map[string]string `json:"labels"`
}

type alertPayload struct {
	Alerts []alert `json:"alerts"`
	Status string  `json:"status"`
}

func sendAlert(payload alertPayload, resolved bool) {
	bot, tg_err := tg.NewBotAPI(config.Config.APIKey)
	if tg_err != nil {
		log.Fatalf("Unable to connect to TG: %s", tg_err)
	}
	log.Printf("Will send message as '%v'", bot.Self.UserName)
	for _, alert := range payload.Alerts {
		alert_name := alert.Labels["alertname"]
		var message string
		if resolved {
			message = fmt.Sprintf("✅ <b>%v</b> ✅\n", alert_name)
		} else {
			message = fmt.Sprintf("🚨 <b>%v</b> 🚨\n", alert_name)
		}
		sorted_keys := make([]string, 0, len(alert.Labels))
		for k := range alert.Labels {
			sorted_keys = append(sorted_keys, k)
		}
		sort.Strings(sorted_keys)
		for _, label := range sorted_keys {
			if label == "alertname" {
				continue
			}
			message += fmt.Sprintf("%v: %v\n", label, alert.Labels[label])
		}
		tg_message := tg.NewMessage(int64(config.Config.ChannelID), message)
		tg_message.ParseMode = "HTML"

		_, err := bot.Send(tg_message)
		if err != nil {
			log.Printf("Unable to send message to telegram: %v", err.Error())
			metrics.RegisterFailure()
			continue
		}
		metrics.RegisterSuccess()
	}
}

func handleRelay(response http.ResponseWriter, request *http.Request) {
	var alertPayload alertPayload
	err := json.NewDecoder(request.Body).Decode(&alertPayload)
	if err != nil {
		log.Printf("Unable to deserialize request: %v", err.Error())
		http.Error(response, err.Error(), http.StatusBadGateway)
		return
	}

	if alertPayload.Status != "resolved" && alertPayload.Status != "firing" {
		log.Printf("Unrecognized alert status: %v", alertPayload.Status)
		http.Error(response, err.Error(), http.StatusBadGateway)
		return
	}
	sendAlert(alertPayload, alertPayload.Status == "resolved")

	response.WriteHeader(http.StatusOK)
}

func main() {
	metrics.Attach()
	http.HandleFunc("/alert", handleRelay)
	log.Println("Listening on port 8080")
	http_err := http.ListenAndServe(":8080", nil)
	if http_err != nil {
		log.Fatalf("Unable to serve metrics: %v", http_err)
	}
}
