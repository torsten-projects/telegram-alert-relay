package metrics

import (
	"fmt"
	"log"
	"net/http"
	"telegram-alert-relay/internal/config"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type exposedMetrics struct {
	RelaysCompleted *prometheus.CounterVec
	RelaysFailed    *prometheus.CounterVec
}

var namespace string = "relay"
var subsystem string = "telegram"

var Metrics exposedMetrics = exposedMetrics{
	RelaysCompleted: prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "relays_completed_total",
		Help:      "The total number of completed relays",
	}, []string{
		"channel_id",
	}),
	RelaysFailed: prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "relays_failed_total",
		Help:      "The total number of failed relays",
	}, []string{
		"channel_id",
	}),
}

func Serve() {
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatalf("Unable to serve metrics: %v", err)
	}
}

func Attach() {
	http.Handle("/metrics", promhttp.Handler())
}

func RegisterSuccess() {
	Metrics.RelaysCompleted.WithLabelValues(fmt.Sprint(config.Config.ChannelID)).Inc()
}

func RegisterFailure() {
	Metrics.RelaysFailed.WithLabelValues(fmt.Sprint(config.Config.ChannelID)).Inc()
}
