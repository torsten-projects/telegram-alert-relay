package config

import (
	"log"
	"os"
	"strconv"
)

type Configuration struct {
	APIKey    string
	ChannelID int
}

var Config Configuration = GetConfiguration()

func GetConfiguration() Configuration {
	return Configuration{
		APIKey:    getOrFail("TELEGRAM_TOKEN"),
		ChannelID: intOrFail(getOrFail("TELEGRAM_CHANNEL_ID")),
	}
}

func getOrFail(key string) (value string) {
	value, ok := os.LookupEnv(key)
	if !ok {
		log.Fatalf("Unable to lookup environment variable '%v'!", key)
	}
	return
}

func intOrFail(str string) (value int) {
	value, err := strconv.Atoi(str)
	if err != nil {
		log.Fatalf("Unable to parse '%v' as an int: %v", value, err)
	}
	return
}
